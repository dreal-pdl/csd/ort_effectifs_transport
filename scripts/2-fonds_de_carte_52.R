library(sf)
library(COGiter)
library(tidyverse)

# Transformation de epci_geo (table de cogiter) pour ne garder que les epci des Pays de la Loire
epci_geo_52<-epci_geo %>% 
  inner_join(epci %>% filter(str_detect(REGIONS_DE_L_EPCI,"52")))
epci_geo_52<-st_transform(epci_geo_52,"+proj=longlat + datum=WGS84")

# Transformation de departements_geo (table de cogiter) pour ne garder que les departements des Pays de la Loire
departements_geo_52<-departements_geo %>% 
  filter(DEP %in% c("44","49","53","72","85"))
departements_geo_52<-st_transform(departements_geo_52,"+proj=longlat + datum=WGS84")

# Transformation de communes_geo (table de cogiter) pour ne garder que les communes des Pays de la Loire
communes_geo_52<-communes_geo %>% 
  inner_join(communes %>%
               filter(str_detect(REGIONS_DE_L_EPCI,"52")))
communes_geo_52<-st_transform(communes_geo_52,"+proj=longlat + datum=WGS84")

# Création d'une table donnant le département d'appartenance des EPCI de France
liste_departements_epci<-communes %>% 
  select(EPCI,DEP,NOM_DEP) %>% 
  distinct() %>% 
  mutate_at(vars(DEP,NOM_DEP), funs(as.character(.))) %>% 
  group_by(EPCI) %>% 
  mutate(DEP=paste0(DEP, collapse = "-"),
         NOM_DEP=paste0(NOM_DEP, collapse = "-")) %>% 
  ungroup %>% 
  distinct

# Sauvegarde des tables créées
save(communes_geo_52,
     epci_geo_52,
     departements_geo_52,
     liste_departements_epci,
     file="app/fonds_de_carte_52.RData")


