<style>
body {
  font-family: 'Marianne', sans-serif;
}
</style>
## Le secteur des transports

### La nomenclature NAF (révision 2)


La nomenclature d'activités française (NAF), élaborée par l'INSEE, constitue le répertoire statistique national des activités économiques. La nouvelle version de cette nomenclature, la NAF révision 2 (NAF rév. 2), est entrée en vigueur le 1er janvier 2008. Elle a remplacé la précédente version, la NAF révision 1 qui datait de 2003.

Cette opération, dont l'un des objectifs était de favoriser les comparaisons internationales, s'est inscrite dans un vaste processus de révision aux niveaux mondial et européen. La NAF a dorénavant la même structure que la nomenclature d'activités de la Communauté européenne (NACE rév. 2) mais elle comporte un niveau supplémentaire, celui des sous-classes.


La NAF révision 2 comporte 732 positions élémentaires (ou sous-classes). La codification des sous-classes s'effectue sur cinq positions : quatre chiffres et une lettre. C'est sous cette forme qu'apparaît le code APE (Activité Principale Exercée) attribué par l'INSEE aux entreprises et aux établissements.

La NAF révision 2 est organisée en plusieurs niveaux hiérarchiques imbriqués. Les plus utilisés sont les suivants :

-   les grands secteurs (au nombre de 4 : agriculture, industrie, construction et tertiaire),

-   les sections (au nombre de 21, numérotées de A à U),

-   les divisions (au nombre de 88, qui sont constituées à partir des deux premières positions du code APE),

-   les groupes (au nombre de 272),

-   les classes (au nombre de 615).

Les niveaux sont dénommés "A xx" où xx représente le nombre de postes (de A 4 à A 732).


### Le secteur des transports et ses principales composantes

Le secteur des transports et de l'entreposage constitue une des 21 sections de la NAF révision 2, la section H. Elle comprend trente sous-classes, de 49.10Z à 52.29B. Selon la notice explicative de l'INSEE, elle « couvre les activités liées au transport, régulier ou non, de passagers et de marchandises, par rail, par route, par conduites, par eau ou par air et les activités connexes, telles que l'exploitation des infrastructures de transport, la manutention du fret, l'entreposage, etc. Cette section comprend la location de matériel de transport avec chauffeur ou pilote. Elle comprend également les activités de poste et de courrier ».

Cette définition des transports en tant qu'activité économique est différente de l'approche par métier. Ainsi, une secrétaire d'une compagnie aérienne fait partie du secteur des transports ; par contre, un conducteur de poids lourds d'une entreprise de travaux publics est ventilé dans le secteur de la construction (dont dépend l'activité principale exercée par l'établissement employeur).

Par rapport à la NAF révision 1, les sous-classes 79.11Z (activités des agences de voyage), 79.12Z (activités des voyagistes) et 79.90Z (autres services de réservation et activités connexes) sont sorties du secteur des transports et font désormais partie de celui des services (division 79). À l'inverse, les sous-classes 53.10Z (activités de poste dans le cadre d'une obligation de service universel) et 53.20Z (autres activités de courrier) ont été intégrées dans le champ des transports.

Dans le cadre de la présente application, le secteur des transports et de l'entreposage a été scindé en neuf catégories.


<b>Transports routiers de voyageurs</b><br> <font color="#00bebe">49.31Z</font> Transports urbains et suburbains de voyageurs<br> <font color="#00bebe">49.32Z</font> Transports de voyageurs par taxis<br> <font color="#00bebe">49.39A</font> Transports routiers réguliers de voyageurs<br> <font color="#00bebe">49.39B</font> Autres transports routiers de voyageurs<br>

<b>Transports routiers de marchandises</b><br> <font color="#00bebe">49.41A</font> Transports routiers de fret interurbains<br> <font color="#00bebe">49.41B</font> Transports routiers de fret de proximité<br> <font color="#00bebe">49.41C</font> Location de camions avec chauffeur<br> <font color="#00bebe">49.42Z</font> Services de déménagement<br> <font color="#00bebe">52.29A</font> Messagerie, fret express<br>

<b>Transports ferroviaires</b><br> <font color="#00bebe">49.10Z</font> Transport ferroviaire interurbain de voyageurs<br> <font color="#00bebe">49.20Z</font> Transports ferroviaires de fret<br>

<b>Transports maritimes et fluviaux</b><br> <font color="#00bebe">50.10Z</font> Transports maritimes et côtiers de passagers<br> <font color="#00bebe">50.20Z</font> Transports maritimes et côtiers de fret<br> <font color="#00bebe">50.30Z</font> Transports fluviaux de passagers<br> <font color="#00bebe">50.40Z</font> Transports fluviaux de fret


<b>Transports aériens</b><br> <font color="#00bebe">51.10Z</font> Transports aériens de passagers<br> <font color="#00bebe">51.21Z</font> Transports aériens de fret<br>

<b>Entreposage, stockage et manutention</b><br> <font color="#00bebe">52.10A</font> Entreposage et stockage frigorifique<br> <font color="#00bebe">52.10B</font> Entreposage et stockage non frigorifique<br> <font color="#00bebe">52.24A</font> Manutention portuaire<br> <font color="#00bebe">52.24B</font> Manutention non portuaire<br>

<b>Services auxiliaires des transports</b><br> <font color="#00bebe">52.21Z</font> Services auxiliaires des transports terrestres<br> <font color="#00bebe">52.22Z</font> Services auxiliaires des transports par eau<br> <font color="#00bebe">52.23Z</font> Services auxiliaires des transports aériens<br> <font color="#00bebe">52.29B</font> Affrètement et organisation des transports<br>

<b>Activités de poste et de courrier</b><br> <font color="#00bebe">53.10Z</font> Activités de poste dans le cadre du service universel<br> <font color="#00bebe">53.20Z</font> Autres activités de poste et de courrier<br>

<b>Autres activités</b><br> <font color="#00bebe">49.39C</font> Téléphériques et remontées mécaniques<br> <font color="#00bebe">49.50Z</font> Transports par conduites<br> <font color="#00bebe">51.22Z</font> Transports spatiaux
