---
editor_options: 
  markdown: 
    wrap: 72
---

#### La démarche et ses deux produits

Repo du projet relatifs aux effectifs salariés dans les transports (double timbre DREAL / ORTM).

Le projet comprend deux parties : une publication markdown portant sur la dernière année et une application Shiny

#### 1 / Le chargement des données

Les données sont placées dans le dossier extdata

##### Données à charger tous les ans :

- _etablissements-et-effectifs-salaries-au-niveau-commune-x-ape-last.csv_ (fichier des données depuis 2006, téléchargé en juin de chaque année sur le site :
https://open.urssaf.fr/explore/dataset/etablissements-et-effectifs-salaries-au-niveau-commune-x-ape-last/export/)


##### Données qui ne changent pas :
-   _data_regions.csv_ (données régionales plus anciennes, 2003 à 2005)
-   _table_ape_transports.xls_ (table des 30 codes et intitulés APE du secteur des transports)
-   _ape_vers_sous_secteur.xls_ (table de correspondance entre les codes APE et les intitulés de sous-secteurs)


#### 2 / La création du datamart

-   ouvrir le projet en cliquant sur le fichier _ort_effectifs_transports.Rproj_

-   placer dans le dossier extdata le fichier csv qui a été téléchargé sur le site de l'URSSAF,

-   lancer le script _1-traitement_des_donnees.R_

-   lancer le script _2-fonds_de_carte_52.R _(afin de prendre en compte les nouveaux zonages du COG)

#### 3 / La génération indépendante de la publication (format RMD) et de l'application Shiny

La création de la publication s'effectue avec le fichier _ort_effectifs_transports.rmd_

L'application Shiny se trouve dans le sous-dossier _app_
